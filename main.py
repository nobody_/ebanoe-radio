#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from radio import EbanoeRadio
from operator import itemgetter
import sys, signal, time

signal.signal(signal.SIGINT, lambda x, y: sys.exit(1))

def termclear():
    sys.stderr.write('\033c')
    sys.stderr.flush() ## redundant?

def printComment(scope, comment):
    title = (post['title'] for post in scope if post['pid'] == comment['pid']).__next__()
    sys.stdout.write('{author:20.20s} ({date:.11s}): {body:140.140s} | {title}\n'.format(
        author = comment['author'],
        date   = comment['date'],
        body   = comment['body'],
        url    = comment['url'],
        title  = title,
        )
    )
    sys.stdout.flush()

radio = EbanoeRadio()

timeout = 10 ## update radio every 10 seconds
timer   = 0
while True:
    ## update scope every 5 minutes
    if timer % (5 * 60) == 0:
        scope = radio.getScope()[:5]
        timer = 0

    comments = []
    for post in scope:
        comments.extend(radio.getComments(post['url']))

    zcomments = sorted(comments, key = itemgetter('cid'), reverse = True)[:10]
    termclear()
    sys.stdout.write('Ebanoe radio:\n\n')
    for comment in zcomments:
        printComment(scope, comment)
    timer += timeout
    time.sleep(timeout)
