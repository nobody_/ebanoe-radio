# -*- coding: utf-8 -*-
import cfscrape
import requests
from bs4 import BeautifulSoup
import time
import datetime
import re

'''
Back to the 00s. Parsing html (sic)
'''

class EbanoeRadio(object):

    def __init__(self, URL = None):
        self.__URL = URL or 'https://ebanoe.it/'
        self.__cookie  = None
        self.__ua      = None
        self.__emoji_pattern = re.compile(u'['
            u'\U0001F600-\U0001F64F'  # emoticons
            u'\U0001F300-\U0001F5FF'  # symbols & pictographs
            u'\U0001F680-\U0001F6FF'  # transport & map symbols
            u'\U0001F1E0-\U0001F1FF'  # flags (iOS)
            ']+', flags=re.UNICODE
        )

    def __scrapeCF(self, url = None):
        try:
            scraper = cfscrape.create_scraper() ## @TODO: proxies?
            self.__cookie, self.__ua = scraper.get_cookie_string(url or self.__URL)
        except Exception as e:
            raise ## @TODO: nothing, that will do

    def __get(self, url = None):
        url = url or self.__URL
        headers = {
            'User-Agent'     : self.__ua,
            'Accept-Encoding': 'gzip',
            'Cache-Control'  : 'max-age=10',
            'Cookie'         : self.__cookie,
        }
        try:
            req = requests.get(url, headers = headers) ## @TODO: proxies?
        except requests.exceptions.RequestException as e:
            raise ## @TODO: nothing, that will do

        if not req.ok:
            self.__scrapeCF(url)
            return self.__get(url)

        req.encoding = 'utf-8'
        return req.text

    def __filterComment(self, cid = None, pid = None, author = None, date = None, url = None, body = None):
        date       = datetime.datetime.strptime(date.strip(), u'%d.%m.%Y в %H:%M')
        timestamp  = int(time.mktime(date.timetuple()))
        _today     = datetime.date.today()
        _yesterday = datetime.date.fromordinal(datetime.date.today().toordinal()-1)

        if date.date() == _today:
            date = date.strftime('%H:%M today')
        elif date.date() == _yesterday:
            date = date.strftime('%H:%M yesterday')
        else:
            date = date.strftime('%H:%M %d/%m/%Y')
        del _today
        del _yesterday

        body = self.__emoji_pattern.sub(r'', body)
        body = body.strip().replace('\n', ' ')

        return {
            'cid'      : cid,
            'pid'      : pid,
            'author'   : author,
            'date'     : date,
            'timestamp': timestamp,
            'url'      : url,
            'body'     : body,
        }

    def getScope(self):
        html = self.__get()
        bs   = BeautifulSoup(html, 'html.parser')

        htmlPosts = bs.findAll('div', attrs = {'class': 'post'})

        posts = []
        for htmlPost in htmlPosts:
            _entry  = htmlPost.find(attrs = {'class': 'entry-title'})

            post_id = list( filter(lambda x: x.startswith('post-') and x != 'post-item', htmlPost['class']) )
            post_id = int(post_id.pop().split('-')[-1])
            title   = _entry.text
            url     = _entry.find('a')['href']
            posts.append({
                'pid'  : post_id,
                'title': title,
                'url'  : url,
            })
        return posts

    def getComments(self, url = None):
        assert url

        html = self.__get(url)
        bs   = BeautifulSoup(html, 'html.parser')
        htmlComments = bs.findAll(attrs = {'class': 'comment-body'})

        post_id = bs.find(attrs = {'class', 'post'})['id']
        post_id = int(post_id.split('-')[-1])

        comments = []
        for htmlComment in htmlComments:
            _meta  = htmlComment.find(attrs = {'class': 'comment-meta'}).find('a')
            cid    = int( htmlComment['id'].split('-')[-1] )
            author = htmlComment.find('cite').text
            url    = _meta['href']
            date   = _meta.text
            body   = '\n'.join([ part.text for part in htmlComment.findAll('p') ]) ## look ma, w/o hands!
            comments.append(self.__filterComment(**{
                'cid'   : cid,
                'pid'   : post_id,
                'author': author,
                'date'  : date,
                'url'   : url,
                'body'  : body,
            }))
        return comments
